package servergame;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.*;
import java.net.Socket;
import platformgame.server.*;
import java.util.ArrayList;

/**
 *
 * @author Samurai_Blue
 */
public class ThreadClient extends Thread {

    private final Socket localSocket;
    private ObjectOutputStream toClient;
    private ObjectInputStream fromClient;

    public ThreadClient(Socket localSocket) {
        this.localSocket = localSocket;
    }

    @Override
    public void run() {
        try (InputStream in = localSocket.getInputStream();
                OutputStream out = localSocket.getOutputStream()) {

            toClient = new ObjectOutputStream(out);
            fromClient = new ObjectInputStream(in);
            Message msg;
            while ((msg = readFromClient()) != null) {
                switch (msg.getType()) {
                    case CONNEXION:
                        Joueur joueur = new Joueur(msg.getTxt(), 0);
                        ServerGame.listPlayer.add(joueur);
                        sendConnexionAccept();
                        writeToClient("Nombre de joueur en ligne :" + ServerGame.nbClient);
                        break;
                    case ASK_LIST:
                        ArrayList<Joueur> list1 = ServerGame.listPlayer;
                        ArrayList<Match> list2 = ServerGame.listMatch;
                        toClient.writeObject(list1);
                        toClient.flush();
                        toClient.writeObject(list2);
                        toClient.flush();

                        break;
                    case PLAY_REQUEST:
                        int score = 0;
                        String pseudo = msg.getTxt();
                        if (ServerGame.currentMatch.getListPlayer().isEmpty()) {
                            ServerGame.currentMatch.addPlayer(new Joueur(pseudo, 0));
                            waitPlayer();
                            String scoreStr = readFromClient().getTxt();
                            ServerGame.currentMatch.getFirstPlayer().setScore(Integer.parseInt(scoreStr));
                            checkWinner();
                        } else if (ServerGame.currentMatch.getListPlayer().size() == 1) {
                            ServerGame.currentMatch.addPlayer(new Joueur(pseudo, 0));
                            waitPlayer();
                            String scoreStr = readFromClient().getTxt();
                            ServerGame.currentMatch.getSecondPlayer().setScore(Integer.parseInt(scoreStr));
                            checkWinner2();
                        } else {
                            ServerGame.listMatch.add(ServerGame.currentMatch);
                            ServerGame.currentMatch = new Match();
                            ServerGame.currentMatch.addPlayer(new Joueur(msg.getTxt(), 0));
                            waitPlayer();
                            String scoreStr = readFromClient().getTxt();
                            ServerGame.currentMatch.getFirstPlayer().setScore(Integer.parseInt(scoreStr));
                            checkWinner();
                        }
                        break;
                    case DECONNEXION:
                        ServerGame.nbClient--;
                        ServerGame.listPlayer.remove(msg.getTxt());
                        break;
                    default:
                        break;
                }
            }
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("Erreur " + ex.getMessage());
        }
    }

    private Message readFromClient() throws IOException, ClassNotFoundException {
        Object object = fromClient.readObject();
        Message msgFrom = (Message) object;
        String txt = msgFrom.getTxt();
        return msgFrom;
    }

    private void sendConnexionAccept() throws IOException {
        String msg = "Vous êtes connectés au serveur";
        Message msgTo = new Message(platformgame.server.TypeMessage.CONNEXION, msg);
        toClient.writeObject(msgTo);
        toClient.flush();
    }

    private void writeToClient(String msg) throws IOException {
        Message msgTo = new Message(platformgame.server.TypeMessage.CONNEXION, msg);
        toClient.writeObject(msgTo);
        toClient.flush();
    }

    private void waitPlayer() {
        try {
            Thread.sleep(6000);
        } catch (InterruptedException ex) {
            System.out.println(ex.getMessage());
        }
        boolean ok = false;
        if (ServerGame.currentMatch.getListPlayer().size() == 2) {
            ok = true;
            try {
                writeToClient("play");
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        } else {
            waitPlayer();
        }
    }

    private void checkWinner() {
        if (ServerGame.currentMatch.getFirstPlayer().getScore()
                > ServerGame.currentMatch.getSecondPlayer().getScore()) {
            try {
                writeToClient("win");
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        } else if (ServerGame.currentMatch.getFirstPlayer().getScore()
                > ServerGame.currentMatch.getSecondPlayer().getScore()) {
            try {
                writeToClient("lose");
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }

        } else {
            try {
                writeToClient("equals");
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private void checkWinner2() {
        if (ServerGame.currentMatch.getFirstPlayer().getScore()
                > ServerGame.currentMatch.getSecondPlayer().getScore()) {
            try {
                writeToClient("lose");
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        } else if (ServerGame.currentMatch.getFirstPlayer().getScore()
                > ServerGame.currentMatch.getSecondPlayer().getScore()) {
            try {
                writeToClient("win");
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }

        } else {
            try {
                writeToClient("equals");
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
