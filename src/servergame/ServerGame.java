package servergame;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import platformgame.server.*;

/**
 *
 * @author Samurai_Blue
 */
public class ServerGame {

    public static void main(String[] args) {
        ServerGame server = new ServerGame(6061);
        server.listening();
    }
    private final int port;
    private final boolean status;
    private ObjectInputStream fromClient;
    private ObjectOutputStream toClient;
    public static int nbClient;
    public static ArrayList<Match> listMatch;
    public static ArrayList<Joueur> listPlayer;
    public static Match currentMatch;

    public ServerGame(int port) {
        this.port = port;
        status = true;
        listMatch= new ArrayList<>();
        listPlayer= new ArrayList<>();
        currentMatch=new Match();
        nbClient=0;
    }

    public void listening() {
        try (ServerSocket serverSock = new ServerSocket(port)) {
            while (status) {
                System.out.println("Server listening ...");
                Socket localSocket = serverSock.accept();
                nbClient++;
                new ThreadClient(localSocket).start();
            }
        } catch (IOException ex) {
            System.out.println("Error listening () " + ex.getMessage());
        }
    }

}